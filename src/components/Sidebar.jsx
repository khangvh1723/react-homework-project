import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import { PATH } from '../config/path'
import './style.scss'

const Sidebar = () => {
    return (
        <div className='d-flex flex-column'>
            <NavLink to={PATH.btdatve}>Bài tập đặt vé</NavLink>
            <NavLink to={PATH.baitapform}>Bài tập Form</NavLink>
        </div>
    )
}

export default Sidebar