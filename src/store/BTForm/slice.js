import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
const initialState = {
    studentList: [],
    studentEdit: undefined,
}

const btFormSlice = createSlice({
    name: 'btForm',
    initialState,
    reducers: {
        addStudent: (state, action) => {
            const index = state.studentList.findIndex(stu => stu.id === action.payload.id)
            if (index === -1) {
                state.studentList.push(action.payload)
                toast.success('Thêm sinh viên thành công')
            }
            else {
                toast.error('Sinh viên đã tồn tại')
            }
        },
        deleteStudent: (state, action) => {
            state.studentList = state.studentList.filter((stu) => stu.id !== action.payload)
            toast.success('Xóa sinh viên thành công')
        },
        editStudent: (state, action) => {
            state.studentEdit = action.payload
        },
        updateStudent: (state, action) => {
            state.studentList = state.studentList.map(stu => {
                if (stu.id === action.payload.id) {
                    return action.payload
                }
                return stu
            })
            // sau khi update thành công
            state.studentEdit = undefined
            toast.success('Cập nhật thành công')
        }
    }
})

export const { actions: btFormActions, reducer: btFormReducer } = btFormSlice