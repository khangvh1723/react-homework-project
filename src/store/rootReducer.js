import { combineReducers } from "redux";
import { BTDatVeReducer } from "./BTDatVe/slice";
import { btFormReducer } from "./BTForm/slice";
export const rootReducer = combineReducers({
    BTDatVe: BTDatVeReducer,
    btForm: btFormReducer,
})