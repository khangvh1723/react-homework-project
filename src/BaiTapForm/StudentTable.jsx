import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { btFormActions } from '../store/BTForm/slice'

const StudentTable = () => {
    const { studentList } = useSelector((state) => state.btForm)
    const dispatch = useDispatch()
    console.log("🚀 ~ file: StudentTable.jsx:6 ~ StudentTable ~ studentList:", studentList)
    return (
        <div className="mt-3">
            <table className='table'>
                <thead className='table-dark'>
                    <tr>
                        <td>Mã SV</td>
                        <td>Họ tên</td>
                        <td>Số điện thoại</td>
                        <td>Email</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        studentList?.map((stu) => (
                            <tr key={stu?.id}>
                                <td>{stu?.id}</td>
                                <td>{stu?.hoTen}</td>
                                <td>{stu?.soDT}</td>
                                <td>{stu?.email}</td>
                                <td>
                                    <button className='btn btn-success me-1' onClick={() => {
                                        dispatch(btFormActions.editStudent(stu))
                                    }
                                    }>Edit</button>
                                    <button className='btn btn-danger' onClick={() => {
                                        dispatch(btFormActions.deleteStudent(stu.id))
                                    }
                                    }>Delete</button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    )
}

export default StudentTable