import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { btFormActions } from '../store/BTForm/slice'

const StudentForm = () => {
    const [formData, setFormData] = useState()
    // form error
    const [formError, setFormError] = useState()

    const { studentEdit } = useSelector((state) => state.btForm)
    console.log("🚀 ~ file: StudentForm.jsx:11 ~ StudentForm ~ studentEdit:", studentEdit)

    const handleFormData = () => (e) => {
        const { name, value, minLength, title, max, validity } = e.target
        let mess
        if (minLength !== -1 && !value.length) {
            mess = `Vui lòng nhập thông tin ${title}`
        } else if (validity.patternMismatch && name == 'email') {
            mess = `Vui lòng nhập đúng email`
        } else if (value.length < minLength) {
            mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`
        } else if (value.length > max && max) {
            mess = `Chỉ được nhập tối đa ${max} ký tự`
        } else if (validity.patternMismatch && ['id', 'soDT'].includes(name)) {
            mess = `Vui lòng nhập ký tự số`
        }
        setFormError({
            ...formError,
            [name]: mess,
        })
        setFormData({
            ...formData,
            [name]: mess ? undefined : value,
        })
    }

    const dispatch = useDispatch()

    useEffect(() => {
        if (!studentEdit) return
        setFormData(studentEdit)
    }, [studentEdit])
    return (
        <form
            onSubmit={(e) => {
                e.preventDefault()

                const elements = document.querySelectorAll('input')
                let formError = {}
                elements.forEach((ele) => {
                    let mess
                    const { name, value, minLength, title, max, validity } = ele
                    if (validity.patternMismatch && name == 'email') {
                        mess = `Vui lòng nhập đúng email`
                    } else if (validity.patternMismatch && ['id', 'soDT'].includes(name)) {
                        mess = `Vui lòng nhập ký tự số`
                    } else if (minLength !== -1 && !value.length) {
                        mess = `Vui lòng nhập thông tin ${title}`
                    } else if (value.length < minLength) {
                        mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`
                    } else if (value.length > max && max) {
                        mess = `Chỉ được nhập tối đa ${max} ký tự`
                    }
                    formError[name] = mess
                })

                let flag = false

                for (let key in formError) {
                    // console.log('value: ', value);
                    if (formError[key]) {
                        flag = true
                        break
                    }
                }
                if (flag) {
                    setFormError(formError)
                    return
                }
                if (studentEdit) {
                    dispatch(btFormActions.updateStudent(formData))
                }
                else {
                    dispatch(btFormActions.addStudent(formData))
                }
            }}
            noValidate
        >
            <div className="row mb-3">
                <div className="col-6">
                    <label htmlFor="InputMaSV" className="form-label">Mã SV</label>
                    <input
                        type="text"
                        className="form-control"
                        name='id'
                        title='id'
                        minLength={7}
                        max={20}
                        pattern='^[0-9]+$'
                        value={formData?.id}
                        onChange={handleFormData()}
                        id="InputMaSV"
                    />
                    <div className="form-text">
                        {formError?.id}
                    </div>
                </div>
                <div className="col-6">
                    <label htmlFor="InputHoTen" className="form-label">Họ tên</label>
                    <input
                        type="text"
                        name='hoTen'
                        title='hoTen'
                        minLength={0}
                        value={formData?.hoTen}
                        onChange={handleFormData()}
                        className="form-control"
                        id="InputHoTen"
                    />
                    <div className="form-text">
                        {formError?.hoTen}
                    </div>
                </div>
            </div>
            <div className="row mb-3">
                <div className="col-6">
                    <label htmlFor="InputSoDT" className="form-label">Số điện thoại</label>
                    <input
                        type="text"
                        className="form-control"
                        id="InputSoDT"
                        name='soDT'
                        title='soDT'
                        minLength={10}
                        max={10}
                        value={formData?.soDT}
                        onChange={handleFormData()}
                        pattern='^[0-9]+$'
                        aria-describedby="hoTenHelpBlock"
                    />
                    <div id="hoTenHelpBlock" className="form-text">
                        {formError?.soDT}
                    </div>
                </div>
                <div className="col-6">
                    <label htmlFor="InputEmail" className="form-label">Email</label>
                    <input
                        type="email"
                        className="form-control"
                        id="InputEmail"
                        name='email'
                        title='email'
                        aria-describedby="emailHelpBlock"
                        minLength={0}
                        value={formData?.email}
                        onChange={handleFormData()}
                    />
                    <div id="emailHelpBlock" className="form-text">
                        {formError?.email}
                    </div>
                </div>
            </div>
            {!studentEdit && <button className="btn btn-success">Thêm sinh viên</button>}
            {studentEdit && <button className='btn btn-warning'>Cập nhật</button>}
        </form >
    )
}

export default StudentForm