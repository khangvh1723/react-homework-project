import React from 'react'
import StudentForm from './StudentForm'
import StudentTable from './StudentTable'

const BaiTapForm = () => {
    return (
        <div className='container'>
            <h1 className='text-white bg-dark py-3 px-3'>Thông tin sinh viên</h1>
            <StudentForm />
            <StudentTable />
        </div >
    )
}

export default BaiTapForm