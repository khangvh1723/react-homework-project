import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import { PATH } from './config/path';
import BTDatVe from './BTDatVe/BTDatVe';
import BaiTapForm from './BaiTapForm/BaiTapForm';
import MainLayout from './layouts/MainLayout';

function App() {
  return (
    <div>
      <Routes>
        <Route element={<MainLayout />}>
          <Route index element={<BTDatVe />} />
          <Route path={PATH.baitapform} element={<BaiTapForm />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
