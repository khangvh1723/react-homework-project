import React from 'react'
import { Outlet } from 'react-router-dom'
import Sidebar from '../components/Sidebar'

const MainLayout = () => {
    return (
        <div className='row'>
            {/* sidebar */}
            <div className="col-lg-2">
                <Sidebar />
            </div>
            {/* content */}
            <div className="col-lg-10">
                <Outlet />
            </div>
        </div>
    )
}

export default MainLayout