import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BTDatVeActions } from '../store/BTDatVe/slice'

const Result = () => {
    const { chairBookings, chairBookeds } = useSelector((state) => state.BTDatVe)
    const dispatch = useDispatch()
    return (
        <div>
            <h3 className='text-center'>DANH SÁCH GHẾ BẠN CHỌN</h3>
            <div className='mt-5'>
                <p><span className='text-bg-warning px-3 py-1 me-2'></span> Ghế đã đặt</p>
                <p><span className='text-bg-success px-3 py-1 me-2'></span> Ghế đang chọn</p>
                <p><span className='text-bg-white px-3 py-1 border me-2'></span> Ghế chưa chọn</p>
            </div>
            <table className='table table-bordered'>
                <thead>
                    <tr>
                        <th>Số ghế</th>
                        <th>Giá</th>
                        <th>Hủy</th>
                    </tr>
                </thead>
                <tbody>
                    {chairBookings.map((ghe) => {
                        return <tr key={ghe.soGhe}>
                            <td>{ghe.soGhe}</td>
                            <td>{ghe.gia}</td>
                            <td>
                                <button className='btn' onClick={() => {
                                    dispatch(BTDatVeActions.setChairBookings(ghe))
                                }}><i class="fa fa-times text-danger"></i></button>
                            </td>
                        </tr>
                    })}
                    {/* Tính tổng tiền */}
                    <tr>
                        <td>Tổng tiền</td>
                        <td>{chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}</td>
                    </tr>
                </tbody>
            </table>
            <button className='btn btn-success' onClick={() => {
                dispatch(BTDatVeActions.setChairBookeds())
            }}>Thanh toán</button>
        </div>
    )
}

export default Result