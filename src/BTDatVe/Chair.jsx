import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BTDatVeActions, BTDatVeReducer } from '../store/BTDatVe/slice'
import cn from 'classnames'
import './styles.scss'

const Chair = ({ ghe }) => {
    const dispatch = useDispatch()
    const { chairBookings, chairBookeds } = useSelector((state) => state.BTDatVe)
    return (
        <button
            className={cn('btn btn-outline-dark Chair', {
                'booking': chairBookings.find((e) => e.soGhe === ghe.soGhe),
                booked: chairBookeds.find((e) => e.soGhe === ghe.soGhe),
            })}
            key={ghe.soGhe} style={{ width: '50px' }}
            onClick={() => {
                dispatch(BTDatVeActions.setChairBookings(ghe))
            }}
        > {ghe.soGhe}</button >
    )
}

export default Chair